from flask import Flask, render_template, request, jsonify
from nltk.tokenize import word_tokenize
import os
import pandas as pd
import numpy as np

app = Flask(__name__)
flag=0
wq=0
data=0
whatflag=0
@app.route("/")
def hello():
    return render_template('chat1.html')
    
@app.route("/about/")
def abt():
    return render_template('about.html')

@app.route("/ask", methods=['POST'])
def ask():
	global flag,wq,data,dg,whatflag
	h=0
	k=0
	n=0
	df = pd.read_csv("dataset.csv")
	df = pd.DataFrame(df, columns = ['name','what','eligibility','certificates','procedure'])
	df = df.set_index(['name'], drop=False)
	#print(df)
	#df = df.set_index(['A'])
	m= request.form['messageText'].strip()
	#print(m)
	a=["അതെ","അല്ല"]
	s=word_tokenize(m)
	print(s,"tokens")
	g=df.index
	v=["അര്‍ഹത","അര്‍ഹതയെകുറിച്ച്","അര്‍ഹതയെപറ്റി","മാനദണ്ഡങ്ങള്‍","മാനദണ്ഡം","മാനദണ്ഡങ്ങളെപറ്റി","മാനദണ്ഡങ്ങളെകുറിച്ച്", 		"മാനദണ്ഡത്തെപറ്റി","മാനദണ്ഡത്തെകുറിച്ച്"]												
	b=["രേഖകള്‍","രേഖകളെപറ്റി","രേഖകളെകുറിച്ച്","സര്‍ട്ടിഫിക്കറ്റുകള്‍","സര്‍ട്ടിഫിക്കറ്റുകളെപറ്റി","സര്‍ട്ടിഫിക്കറ്റുകളെകുറിച്ച്"]
	c=["നടപടിക്രമങ്ങള്‍","നടപടി ക്രമങ്ങള്‍","നടപടിക്രമങ്ങളെപറ്റി","നടപടിക്രമങ്ങളെകുറിച്ച്","അപേക്ഷിക്കാനുള്ള നടപടിക്രമം"]
	k=list(set(s).intersection(set(v)))
	print(k)
	h=list(set(s).intersection(set(b)))
	print(h)
	n=list(set(s).intersection(set(c)))
	print(n)
	print(whatflag,"this is whatflag")
	if flag==0 and m not in g:
		ad="താങ്കള്‍ പറയുന്നത് വ്യകതമായില്ല%%"
	elif flag==0 or flag==1 and m in g:
		ad=m+"പദ്ധതിയെ പറ്റി ആണൊ അറിയേണ്ടത്?(അതെ/അല്ല)%%"
		#data=df[df.name==m]
		data=df.loc[m]
		print(data)
		flag=1
		print(flag)
	if flag==1 and m=="അതെ":
		ad=data.what+"ഈ  പദ്ധതിയെ പറ്റിയുള്ള കൂടുതല്‍ വിവരങ്ങള്‍ക്ക് താഴെ തന്നിരിക്കുന്നവയില്‍ നിന്നും തെരഞ്ഞെടുക്കുക%%അര്‍ഹത%%രേഖകള്‍%%നടപടിക്രമങ്ങള്‍%%താല്‍പര്യമില്ല.%%"
		print(ad)
		flag=2
		whatflag=1
		print("flag2")
		print(flag)
		
	if (flag==2 or flag==3) and whatflag==0:
		print("this is working")
		print(type(n))
		#for i in s:
		if len(k)!=0 and flag==2:
			print(data.eligibility)
			if pd.isnull(data.eligibility):
				ad="അര്‍ഹതയെ പറ്റിയുള്ള വിവരങ്ങള്‍ ലഭ്യമല്ല."+"ഈ  പദ്ധതിയെ പറ്റിയുള്ള കൂടുതല്‍ വിവരങ്ങള്‍ക്ക് താഴെ തന്നിരിക്കുന്നവയില്‍ നിന്നും തെരഞ്ഞെടുക്കുക%%രേഖകള്‍%%നടപടിക്രമങ്ങള്‍%%താല്‍പര്യമില്ല.%%"
			else:
				ad=data.eligibility+"ഈ  പദ്ധതിയെ പറ്റിയുള്ള കൂടുതല്‍ വിവരങ്ങള്‍ക്ക് താഴെ തന്നിരിക്കുന്നവയില്‍ നിന്നും തെരഞ്ഞെടുക്കുക%%രേഖകള്‍%%നടപടിക്രമങ്ങള്‍%%താല്‍പര്യമില്ല.%%"
		elif len(h)!=0 and flag==2:
			print(data.certificates)
			if pd.isnull(data.certificates):
				ad="രേഖകളെ പറ്റിയുള്ള വിവരങ്ങള്‍ ലഭ്യമല്ല."+"ഈ  പദ്ധതിയെ പറ്റിയുള്ള കൂടുതല്‍ വിവരങ്ങള്‍ക്ക് താഴെ തന്നിരിക്കുന്നവയില്‍ നിന്നും തെരഞ്ഞെടുക്കുക%%അര്‍ഹത%%നടപടിക്രമങ്ങള്‍%%താല്‍പര്യമില്ല%%"
			else:
				ad=data.certificates+"ഈ  പദ്ധതിയെ പറ്റിയുള്ള കൂടുതല്‍ വിവരങ്ങള്‍ക്ക് താഴെ തന്നിരിക്കുന്നവയില്‍ നിന്നും തെരഞ്ഞെടുക്കുക%%അര്‍ഹത%%നടപടിക്രമങ്ങള്‍%%താല്‍പര്യമില്ല%%"
			flag=2
		elif len(n)!=0 and flag==2:
			print(data.procedure)
			if pd.isnull(data.procedure):
				ad=" നടപടിക്രമങ്ങളെ പറ്റിയുള്ള വിവരങ്ങള്‍ ലഭ്യമല്ല."+"ഈ  പദ്ധതിയെ പറ്റിയുള്ള കൂടുതല്‍ വിവരങ്ങള്‍ക്ക് താഴെ തന്നിരിക്കുന്നവയില്‍ നിന്നും തെരഞ്ഞെടുക്കുക%%അര്‍ഹത%%രേഖകള്‍%%താല്‍പര്യമില്ല%%"
			else:
				ad=data.procedure+"ഈ  പദ്ധതിയെ പറ്റിയുള്ള കൂടുതല്‍ വിവരങ്ങള്‍ക്ക് താഴെ തന്നിരിക്കുന്നവയില്‍ നിന്നും തെരഞ്ഞെടുക്കുക%%അര്‍ഹത%%രേഖകള്‍%%താല്‍പര്യമില്ല%%"	
			flag=2
		elif  len(k)==0 and len(n)==0 and len(h)==0 and flag==2:
			ad="മറ്റെതെങ്കിലും പദ്ധതിയെ പറ്റി അറിയണമോ?(അറിയണം/അറിയണ്ട)%%"
			flag=3
		elif flag==3 and m=="അറിയണം":
			ad="പദ്ധതിയുടെ പേര് പറയുക%%"
			flag=0
		elif flag==3 and m=="അറിയണ്ട":
			ad="നന്ദി%%"
			flag=0
		
	if flag==1 and m=="അല്ല":
		ad="പദ്ധതിയുടെ പേര്  വ്യകതമായി പറയാമോ%%"
		flag=0
	
	whatflag=0	
	#otp = query_search(y)
	return jsonify({'status':'OK','answer':ad})


if __name__ == "__main__":
    app.run(host='0.0.0.0',port=5000)
#print(y)
